#!/usr/bin/python

import asyncio

# local lib
import helper

async def bot(last_update_id):
    update = await helper.getUpdates()
    next_update_id = last_update_id + 1
    for input_message in update['result']:
        if input_message['update_id'] == next_update_id:
            # reply to
            reply_to = input_message['message']['from']['id']
            message_text = input_message['message']['text']
            print (input_message)
            reply = await helper.sendMessage(chat_id=reply_to, text=message_text)
            print (reply)
            # update the last_update_id
            return True
    return False


async def main(last_update_id):
    temp = await bot(last_update_id)
    return temp

async def setup():
    # get the update id
    print ('Setting Up the Bot')
    # get the updates
    update = await helper.getUpdates()
    # leave all message and wait for new messages
    return update['result'][-1]['update_id']

async def bot_wait() -> None:
    # bot waiting
    await asyncio.sleep(1)

if __name__ == "__main__":
    # start the asyncio loop
    loop = asyncio.get_event_loop()
    # starting message id
    last_update_id = loop.run_until_complete(setup())

    print('Running Bot, Use Ctrl+C to stop bot')
    # run infinite loop
    while True:
        result = loop.run_until_complete(main(last_update_id))
        if result == True:
            # update the last id
            last_update_id = last_update_id + 1
        else:
            # wait for next message
            loop.run_until_complete(bot_wait())
