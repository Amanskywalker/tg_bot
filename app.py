import requests
from fastapi import FastAPI

# local lib
import config

app = FastAPI()

@app.get('/')
async def root():
    return {"message": "Hello World"}

@app.get("/send/{message}")
async def send_message(message):
    send_text = 'https://api.telegram.org/bot' + config.BOT_TOKEN + '/sendMessage?chat_id=' + config.BOT_CHAT_ID + '&parse_mode=Markdown&text=' + message
    response = requests.get(send_text)
    return response.json()
