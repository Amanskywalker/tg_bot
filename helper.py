import aiohttp

# local lib
import config

# Get the update
# TODO: Implement Long Polling
async def getUpdates():
    url = 'https://api.telegram.org/bot'+ config.BOT_TOKEN +'/getUpdates'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            # return the json file if response is 200
            if resp.status == 200:
                return await resp.json()

async def sendMessage(chat_id, text, reply_to_message_id=None):
    message = {
        'chat_id'             : chat_id,
        'text'                : text,
        'reply_to_message_id' : reply_to_message_id
    }
    print (message)
    url = 'https://api.telegram.org/bot'+ config.BOT_TOKEN +'/sendMessage'
    async with aiohttp.ClientSession() as session:
        async with session.post(url, data=message) as resp:
            # return the json file if response is 200
            if resp.status == 200:
                return await resp.json()
