# Echo for telegram bot
This is a simple telegram bot which repeates the text message which is sent to it.

### Before starting
Get the bot token from @BotFather and add it to `BOT_TOKEN` in `config.py`

Once the bot is create vist the bot to click on start to start it.

To get the chat id go to https://api.telegram.org/bot<BOT_TOKEN>/getUpdates to get the chats which are sent to bot.

### Installation
Run `python -m pipenv install` to install the required packages.

### Execution
Run `python start_bot.py` to start the echo bot.

### Server
Run `hypercorn main:app --reload` to start the web server to send custom message, before sending the custome messages you need to add the `BOT_CHAT_ID` in `config.py` file
